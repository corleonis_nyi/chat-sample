package com.corleonis.chat

import android.graphics.Bitmap
import com.github.bassaer.chatmessageview.model.IChatUser

data class User(
    val userName: String = "",
    val classID: String = ""
) : IChatUser {
    override fun getName(): String = userName

    override fun getIcon(): Bitmap? {
        return null
    }

    override fun getId(): String = classID

    override fun setIcon(bmp: Bitmap) {
    }
}