package com.corleonis.chat

import android.app.Application

class App: Application(){
    companion object {
        var myID: String = ""
    }
}

const val FIRESTORE_TAG = "Firestore"