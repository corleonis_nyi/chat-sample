package com.corleonis.chat

import java.util.*

data class Chat(
    val userID: String = "",
    val comment: String = "",
    val chatDate: ChatDate = getChatDate()
)

data class ChatDate(
    val year: Int = 0,
    val month: Int = 0,
    val date: Int = 0,
    val hour: Int = 0,
    val minute: Int = 0,
    val second: Int = 0
)


fun getCalendar(chatDate: ChatDate): Calendar{
    val calendar = Calendar.getInstance()
    chatDate.run {
        calendar.set(
            year,
            month,
            date,
            hour,
            minute,
            second
        )
    }
    return calendar
}


fun getChatDate(): ChatDate{
    val calendar = Calendar.getInstance()
    return ChatDate(
        calendar.get(java.util.Calendar.YEAR),
        calendar.get(java.util.Calendar.MONTH),
        calendar.get(java.util.Calendar.DATE),
        calendar.get(java.util.Calendar.HOUR_OF_DAY),
        calendar.get(java.util.Calendar.MINUTE),
        calendar.get(java.util.Calendar.SECOND)
    )
}