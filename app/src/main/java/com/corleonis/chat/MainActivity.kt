package com.corleonis.chat

import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.github.bassaer.chatmessageview.model.Message
import com.github.bassaer.chatmessageview.view.ChatView



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messenger)


        val mChatView = findViewById<View>(R.id.chat_view) as ChatView

        //Set UI parameters if you need
        mChatView.apply {
            setRightBubbleColor(ContextCompat.getColor(this@MainActivity, R.color.green500))
            setLeftBubbleColor(Color.WHITE)
            setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.blueGray500))
            setSendButtonColor(ContextCompat.getColor(this@MainActivity, R.color.cyan900))
            setSendIcon(R.drawable.ic_action_send)
            setRightMessageTextColor(Color.WHITE)
            setLeftMessageTextColor(Color.BLACK)
            setUsernameTextColor(Color.WHITE)
            setSendTimeTextColor(Color.WHITE)
            setDateSeparatorColor(Color.WHITE)
            setInputTextHint(" new message...")
            setMessageMarginTop(5)
            setMessageMarginBottom(5)

            //Click Send Button
            setOnClickSendButtonListener(View.OnClickListener {

                val message = Message.Builder()
                    .setUser(User())
                    .setRight(true)
                    .setText(mChatView.inputText)
                    .hideIcon(true)
                    .build()

                mChatView.send(message)

                //Reset edit text
                mChatView.inputText = ""
            })
        }

    }

}
